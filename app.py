#%%
from flask import Flask, make_response, render_template, request
from routes.hello import hello_bp
from util.response_builder import response_builder

# %%
app = Flask(__name__)
app.register_blueprint(hello_bp)

@app.route('/')
@app.route('/swagger.json')
def swagger():
    try:
        res = response_builder(render_template('swagger.json'), 200)
        return res
    except:
        res = make_response("response failure", 500)
        return res