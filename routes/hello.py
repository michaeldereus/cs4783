from flask import Blueprint, make_response, request
import sys
sys.path.append('../')
from util.response_builder import response_builder

hello_bp = Blueprint('hello', __name__)

@hello_bp.route('/hello', methods=['GET'])
def hello_world():
    if request.method == 'GET':
        return response_builder({"message":"hello yourself"},200)
    else:
        return make_response("response failure", 500)