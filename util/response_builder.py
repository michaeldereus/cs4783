from flask import make_response

def response_builder(res, res_num):
    response = make_response(res, res_num)
    response.headers['Content-Type'] = 'application/json'
    response.headers['Access-Control-Allow-Methods'] = '*'
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, api_key, Authorization'
    return response